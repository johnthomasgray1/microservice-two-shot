from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
   # id = models.PositiveIntegerField(primary_key=True, serialize=True)
   href = models.CharField(max_length=200, unique=True)
   closet_name = models.CharField(max_length=150)


class Shoe(models.Model):
    
   manufacturer = models.CharField(max_length=150)
   model = models.CharField(max_length=150)
   color = models.CharField(max_length=150)
   picture_url = models.URLField(null=True)

   bin = models.ForeignKey(
      BinVO,
      related_name="shoes",
      on_delete=models.PROTECT
   )
   

   def __str__(self):
      return f"{self.model}, {self.manufacturer}"
   
   class Meta:
      ordering = ["manufacturer", "model", "color", "picture_url"]
   

