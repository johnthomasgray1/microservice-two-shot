from django.contrib import admin
from hats_rest.models import Hat, LocationVO

@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
  list_display = [
    "id",
    "fabric",
    "style",
    "color",
    "picture_url",
    "location"
  ]

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
  list_display = [
    "name",
    "import_href",
  ]
