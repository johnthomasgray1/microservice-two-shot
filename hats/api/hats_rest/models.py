from django.db import models

class LocationVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  name = models.CharField(max_length=200)

class Hat(models.Model):
  fabric = models.CharField(max_length=100)
  style = models.CharField(max_length=100)
  color = models.CharField(max_length=50)
  picture_url = models.URLField(null=True)

  location = models.ForeignKey(
      LocationVO,
      related_name="hats",
      on_delete=models.PROTECT,
  )

  def __str__(self):
    return f"{self.color} {self.style}"

  class Meta:
    ordering = ("fabric", "style", "color", "picture_url")
