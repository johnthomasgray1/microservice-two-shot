import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

export default function HatList() {
  const [hats, setHats] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }


  const handleDelete = async (e) => {
    const id = e.target.id;
    console.log("id" + id);
    const url = `http://localhost:8090/api/hat/${id}`
    const fetchConfig = {
      method: 'DELETE',
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok){
      fetchData();
    } else {
      console.error("Response not okay");
    }
  }

  useEffect(() => {
    fetchData();
  }, [])


  return (
    <>
      <table className="tabel table-striped">
        <thead>
          <tr>
            <th>Photo</th>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Location</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={ hat.id }>
                <td>
                  <img src={ hat.picture_url } style={{ width: '150px' }} className="img-fluid" alt="Hat"/>
                </td>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location["name"] }</td>
                <td>
                <button onClick={handleDelete} id={ hat.id }>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
    </table>
    <div>
      <p></p>
      <Link className="btn btn-primary" to="/hats/new">
        Create a hat!
      </Link>
    </div>
  </>
  )
}
