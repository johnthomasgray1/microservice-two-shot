import React, { useEffect, useState } from 'react';

export default function HatForm() {

  const [locations, setLocations] = useState([])

  const initialState = {
    fabric : '',
    style : '',
    color : '',
    picture_url : '',
    location : '',
  }

  const [formData, setFormData] = useState(initialState)

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
      console.log(data.locations);
    }

  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = 'http://localhost:8090/api/hats/'

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData(initialState);
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    console.log(value);
    setFormData({
      ...formData,
      [inputName]: value
    });
  }
  return (

    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={formData.fabric}/>
              <label htmlFor="fabric">Fabric</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" value={formData.style}/>
              <label htmlFor="style">Style</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color}/>
              <label htmlFor="color">Color</label>
            </div>

            <div className="mb-3">
              <label htmlFor="picture_url">Picture URL</label>
              <textarea onChange={handleFormChange} placeholder="URL" required type="text" id="picture_url" rows="3" name="picture_url" className="form-control" value={formData.picture_url}></textarea>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="location" id="location" className="form-select" value={formData.location}>
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
