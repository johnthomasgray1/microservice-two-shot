import React, {useState, useEffect} from 'react';


function ShoesList() {

    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    const handleDelete = async (event) => {
        const id = event.target.id;
        const url = `http://localhost:8080/api/shoes/${ id }/`;
        const fetchConfig = {
            method: 'DELETE'
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchData();
        } else {
            console.error('Failed to delete shoe')
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <table className="table table-image">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Manufacturer</th>
                                <th>Closet</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {shoes.map(
                                shoe => {
                                    return (
                                        <tr key={shoe.id}>
                                            <td className="w-25">
                                                <img src={shoe.picture_url} className="img-fluid img-thumbnail" alt="Shoe"></img>
                                            </td>
                                            <td>{shoe.model}</td>
                                            <td>{shoe.color}</td>
                                            <td>{shoe.manufacturer}</td>
                                            <td>{shoe.bin}</td>
                                            <td>
                                                <button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    )
                                }
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                // <table className="table table-striped">
                //     <thead>
                //         <tr>
                //             <th></th>
                //             <th>Model</th>
                //             <th>Color</th>
                //             <th>Manufacturer</th>
                //             <th>Closet</th>
                //             <th></th>
                //         </tr>
                //     </thead>
                //     <tbody>
                //         {shoes.map(
                //             shoes => {
                //                 return (
                //                     <tr key={shoes.id}>
                //                         <td>{shoes.model}</td>
                //                         <td>{shoes.color}</td>
                //                         <td>{shoes.manufacturer}</td>
                //                         <td>{shoes.bin}</td>
                //                         <td>
                //                             <button onClick={handleDelete} id={shoes.id} className="btn btn-danger">Delete</button>
                //                         </td>
                //                     </tr>
                //                 );
                //             }
                //         )}
                //     </tbody>
                // </table>
    )
}

export default ShoesList;