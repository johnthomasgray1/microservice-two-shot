import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [model, setModel] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.model = model;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setModel('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');

        }
    }
        const handleModelChange = (event) => {
            const value = event.target.value;
            setModel(value);
        }

        const handleManufacturerChange = (event) => {
            const value = event.target.value;
            setManufacturer(value);
        }

        const handleColorChange = (event) => {
            const value = event.target.value;
            setColor(value);
        }

        const handleBinChange = (event) => {
            const value = event.target.value;
            setBin(value);
        }

        const handlePictureChange = (event) => {
            const value = event.target.value;
            setPictureUrl(value)
        }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={model}
                            onChange={handleModelChange}
                            placeholder="Model"
                            required type="text"
                            name="model"
                            id="model"
                            className="form-control">
                            </input>
                            <label htmlFor="model">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={manufacturer}
                            onChange={handleManufacturerChange}
                            placeholder="Manufacturer"
                            required type="text"
                            name="manufacturer"
                            id="manufacturer"
                            className="form-control">
                            </input>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color}
                            onChange={handleColorChange}
                            placeholder="Color"
                            required type="text"
                            name="color"
                            id="color"
                            className="form-control">
                            </input>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureUrl}
                            onChange={handlePictureChange}
                            placeholder="Shoe"
                            required type="text"
                            name="image"
                            id="image"
                            className="form-control">
                            </input>
                            <label htmlFor="image">Image Url</label>
                        </div>
                        <div className="mb-3">
                            <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                                <option value="">Choose Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
    
}

export default ShoeForm;